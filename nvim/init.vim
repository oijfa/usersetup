"VIM-PLUGIN - List all vim plugins (Make sure you use single quotes)
call plug#begin('~/.vim/plugged')
	Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
	Plug 'rbong/vim-crystalline'
	Plug 'easymotion/vim-easymotion'
	Plug 'dense-analysis/ale'
	"if has('nvim')
	"	Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
	"else
	"	Plug 'Shougo/deoplete.nvim'
	"	Plug 'roxma/nvim-yarp'
	"	Plug 'roxma/vim-hug-neovim-rpc'
	"endif
	Plug 'tpope/vim-fugitive'
	Plug 'tomasr/molokai'
	Plug 'fmoralesc/molokayo'
	Plug 'nanotech/jellybeans.vim'
call plug#end()

"Base
syntax on
colorscheme jellybeans
set tabstop=2
set softtabstop=2
set shiftwidth=2
set noexpandtab
set smarttab
set autoread
set autoindent
set smartindent
set number
set showcmd
set nowrap
set backspace=indent,eol,start
set shortmess=tI
set list listchars=tab:»\ ,extends:›,precedes:‹,nbsp:·,trail:·
"set mouse=a

"WildMode
set wildmode=list:longest
set wildmenu

"NERDTree
map <C-n> :NERDTreeToggle<CR>

"Crystalline config
function! StatusLine(current, width)
  let l:s = ''

  if a:current
    let l:s .= crystalline#mode() . crystalline#right_mode_sep('')
  else
    let l:s .= '%#CrystallineInactive#'
  endif
  let l:s .= ' %f%h%w%m%r '
  if a:current
    let l:s .= crystalline#right_sep('', 'Fill') . ' %{fugitive#head()}'
  endif

  let l:s .= '%='
  if a:current
    let l:s .= crystalline#left_sep('', 'Fill') . ' %{&paste ?"PASTE ":""}%{&spell?"SPELL ":""}'
    let l:s .= crystalline#left_mode_sep('')
  endif
  if a:width > 80
    let l:s .= ' %{&ft}[%{&fenc!=#""?&fenc:&enc}][%{&ff}] %l/%L %c%V %P '
  else
    let l:s .= ' '
  endif

  return l:s
endfunction

function! TabLine()
  let l:vimlabel = has('nvim') ?  ' NVIM ' : ' VIM '

  "ALE Configuration
  let l:counts = ale#statusline#Count(bufnr(''))
  let l:all_errors = l:counts.error + l:counts.style_error
  let l:all_non_errors = l:counts.total - l:all_errors

  let l:clean_status = l:counts.total == 0 ? 'No Errors/Warnings' : printf('%dWarnings | %dErrors', l:all_non_errors, l:all_errors)
  return crystalline#bufferline(10, len(l:vimlabel), 1) . crystalline#right_mode_sep('') . '%=%#CrystallineTab#' . l:clean_status . '%=%#CrystallineTab#' . crystalline#left_mode_sep('') . l:vimlabel
endfunction

let g:crystalline_enable_sep = 1
let g:crystalline_statusline_fn = 'StatusLine'
let g:crystalline_tabline_fn = 'TabLine'
let g:crystalline_theme = 'default'

set showtabline=2
set guioptions-=e
set laststatus=2

"deoplete
"let g:deoplete#enable_at_startup = 1

set statusline=
set statusline+=%m
set statusline+=\ %f
set statusline+=%=
set statusline+=\ %{LinterStatus()}

let g:ale_fixers = {
\ '*': ['remove_trailing_lines', 'trim_whitespace', 'prettier']
\}
let g:ale_completion_enabled = 1
let g:ale_fix_on_save = 1
let g:ale_set_highlights = 0

PLUG_MAN_URL="https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
PLUG_MAN_PATH="$HOME/.local/share/nvim/site/autoload/plug.vim"
mv --help 2>&1 | grep "backup\[=CONTROL\]" 2>&1 1>/dev/null
MV_BACKUP_SUPPORT=$?

echo "Move backup support? $MV_BACKUP_SUPPORT"

# Get path from where setup was launched
MY_PATH="`dirname \"$0\"`"              # relative
MY_PATH="`( cd \"$MY_PATH\" && pwd )`"  # absolutized and normalized
if [ -z "$MY_PATH" ] ; then
	# error; for some reason, the path is not accessible
	# to the script (e.g. permissions re-evaled after suid)
	exit 1  # fail
fi

# Create config folder to keep our stuff
if [[ -d "$HOME/.config/usersetup" ]]; then
	echo "$HOME/.config/usersetup ALREADY EXISTS. Backing up"
		if [ $MV_BACKUP_SUPPORT -eq 0 ]; then
			mv --backup=t $HOME/.config/usersetup $HOME/.config/usersetup.bak
		else
			mv $HOME/.config/usersetup $HOME/.config/usersetup.bak
		fi
else
	echo "$HOME/.config/usersetup not found. Creating directory and copying data"
fi
mkdir -p $HOME/.config/usersetup
shopt -s dotglob # for considering dot files (turn on dot files)
cp -R $MY_PATH/* $HOME/.config/usersetup/
rm $HOME/.config/usersetup/setup.sh
MY_PATH=$HOME/.config/usersetup

# Check for and backup old versions of nvim folder and .bashrc
if [[ -d "$HOME/.config/nvim" ]]; then
	echo "Neovim config directory found: Backing up as ~/.config/nvim.bak"
	if [ $MV_BACKUP_SUPPORT -eq 0 ]; then
		mv --backup=t $HOME/.config/nvim $HOME/.config/nvim.bak
	else
		mv $HOME/.config/nvim $HOME/.config/nvim.bak
	fi
fi
if [[ -f "$HOME/.bashrc" ]]; then
	echo ".bashrc file found: Backing up as ~/.config/.bashrc.bak"
	cp $HOME/.bashrc $HOME/.bashrc.bak
fi

# Link home files to our homefiles git dir
echo "Linking configs"
ln -sf $MY_PATH/nvim $HOME/.config/
ln -sf $MY_PATH/.bashrc $HOME/.bashrc

# Download plug-vim plugin manager
echo "Downloading $PLUG_MAN_URL to $PLUG_MAN_PATH"
curl -fLo $PLUG_MAN_PATH --create-dirs $PLUG_MAN_URL

# Generate vim/nvim compatibility mode
echo "Making compatibility links"
mkdir -p $HOME/.local/share/nvim/site
ln -sf $HOME/.local/share/nvim/site $HOME/.vim
ln -sf $HOME/.config/nvim/init.vim $HOME/.vimrc

# Attempt to use nvim; fallback on vim
echo "Install Plugins"
if [ `command -v "nvim"` ]; then
	echo "Using nvim"
	NVIM_VER="`nvim -v | head -1 | cut -d ' ' -f 2 | cut -d '.' -f 2`"
	if [ "$NVIM_VER" -lt 2 ]; then
		echo "nvim < 0.2.0, using crystalline branch vim-7"
		sed -i "s/crystalline'/crystalline\', \{'branch':'vim-7'\}/" $MY_PATH/nvim/init.vim
	fi
	nvim --headless +PlugInstall +qa
elif [ `command -v "vim"` ]; then
	echo "Using vim"
	vim +PlugInstall +qa
else
	echo "Cannot find neovim or vim; cannot install plugin manager"
fi
echo "Done."

#if [ -f /etc/bash_completion ]; then
#    . /etc/bash_completion
#fi

complete -cf sudo


ls --help 2>&1 | grep "color=\[WHEN\]" -A1 | grep auto 2>&1 1>/dev/null
if [ $? -eq 0 ]; then
	alias ls='ls --color=auto'
fi
PS1='\[\e[0;37m\][\[\e[0;36m\]\u\[\e[1;35m\]@\[\e[0;31m\]\h \[\e[1;34m\]\w\[\e[1;32m\]\[\e[0;37m\]]\[\e[1;32m\]\$ \[\e[m\]\[\e[1;37m\]'
EDITOR="vim"

if [ ! -e /tmp/.esd-${UID} ]; then
        ln -sf /tmp/.esd /tmp/.esd-${UID}
fi
